import React from 'react';

import AzimuthComponent from './AzimuthComponent';

export default {
    title: 'AzimuthComponent',
    component: AzimuthComponent,
    argTypes: {
        requestedAzimuthAngleDegrees: {
            name: "Requested Azimuth Angle (degrees)",
            control: {
                type: 'range',
                min: -360,
                max: 360,
                step: 1
            }
        },
        azimuthAngleDegrees: {
            name: "Azimuth Angle (degrees)",
            control: {
                type: 'range',
                min: -360,
                max: 360,
                step: 1
            }
        },
        requestedAzimuthPowerPercentage : {
            name: "Requested Azimuth Power (%)",
            control: {
                type: 'range',
                min: -100,
                max: 100,
                step: 1
            }
        },
        azimuthPowerPercentage: {
            name: "Azimuth Power (%)",
            control: {
                type: 'range',
                min: -100,
                max: 100,
                step: 1
            }
        },
        sml: {
            name: "Component Size",
            control: {
                type: 'select',
                options: ['s', 'm', 'l']
            }
        }
    },
};

const Template = (args) => <AzimuthComponent {...args} />;

export const Large = Template.bind({});
Large.args = {
    requestedAzimuthAngleDegrees: 15,
    azimuthAngleDegrees: 20,
    requestedAzimuthPowerPercentage: 30,
    azimuthPowerPercentage: 25,
    sml: 'l',
};

export const Medium = Template.bind({});
Medium.args = {
    requestedAzimuthAngleDegrees: 15,
    azimuthAngleDegrees: 20,
    requestedAzimuthPowerPercentage: 30,
    azimuthPowerPercentage: 25,
    sml: 'm',
};

export const Small = Template.bind({});
Small.args = {
    requestedAzimuthAngleDegrees: 15,
    azimuthAngleDegrees: 20,
    requestedAzimuthPowerPercentage: 30,
    azimuthPowerPercentage: 25,
    sml: 's',
};
