import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AzimuthWatchFaceSvgComponent from './svg-components/AzimuthWatchFaceSvgComponent.jsx';
import MainEngineComponent from './MainEngineComponent.jsx';
import ArrowSvgComponent from './svg-components/ArrowSvgComponent.jsx';
export default class AzimuthComponent extends Component {
    render() {
        const { requestedAzimuthAngleDegrees, azimuthAngleDegrees, requestedAzimuthPowerPercentage, azimuthPowerPercentage, sml } = this.props;
        let totalWidth = 256;
        let totalHeight = 256;
        let circlePadding = 24;
        let arrowPaddingX = 120;
        let main_engine_y_padding = 7;
        if (sml === 's') {
            totalWidth = 80;
            totalHeight = 80;
            circlePadding = 8;
            arrowPaddingX = 38;
            main_engine_y_padding = 0;
        } else if (sml === 'l') {
            totalWidth = 512;
            totalHeight = 512;
            circlePadding = 48;
            arrowPaddingX = 240;
            main_engine_y_padding = 14;
        }
        const viewBox = '0 0 ' + totalWidth + ' ' + totalHeight;
        let requested_arrow_rotate_string =
            'rotate(' + requestedAzimuthAngleDegrees + ' ' + totalWidth / 2 + ' ' + totalHeight / 2 + ')';
        return (
            <svg x={0} y={0} width={totalWidth} height={totalHeight} viewBox={viewBox}>
                <AzimuthWatchFaceSvgComponent x={circlePadding} y={circlePadding} sml={sml} />
                <MainEngineComponent
                    x={0}
                    y={main_engine_y_padding}
                    requestedThrottle={requestedAzimuthPowerPercentage}
                    engineThrottle={azimuthPowerPercentage}
                    rotationDegrees={azimuthAngleDegrees}
                    sml={sml}
                />
                <g transform={requested_arrow_rotate_string}>
                    <ArrowSvgComponent x={arrowPaddingX} y={main_engine_y_padding} fillClassName={'blksail-input'} sml={sml} />
                </g>
            </svg>
        );
    }
}
AzimuthComponent.defaultProps = {};

AzimuthComponent.propTypes = {
    requestedAzimuthAngleDegrees: PropTypes.number,
    azimuthAngleDegrees: PropTypes.number,
    requestedAzimuthPowerPercentage: PropTypes.number,
    azimuthPowerPercentage: PropTypes.number,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
