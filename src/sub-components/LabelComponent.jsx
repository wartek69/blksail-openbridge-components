import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class LabelComponent extends Component {
  render() {
    const { labelText, value, unitText, isRequestedValue, sml } = this.props;

    let fontSizeValue = 32;
    let fontWeightValue = 600;
    let lineHeightValue = '48px';
    let fontSizeLabel = 16;
    let lineHeightLabel = '22px';
    let labelTextBottomMargin = '-9px';
    let textPaddingLeft = '4px';
    let unitMarginTop = '-10px';
    if (sml === 'l') {
      fontSizeValue = 64;
      lineHeightValue = '88px';
      fontSizeLabel = 24;
      lineHeightLabel = '42px';
      labelTextBottomMargin = '-25px';
      textPaddingLeft = '8px';
      unitMarginTop = '-24px';
    }

    let valueTextStyle = {
      fontSize: fontSizeValue,
      fontWeight: fontWeightValue,
      fontStyle: 'normal',
      fontFamily: 'Open Sans',
      lineHeight: lineHeightValue,
      margin: 0,
    };

    let labelTextStyle = {
      fontSize: fontSizeLabel,
      fontStyle: 'normal',
      fontFamily: 'Open Sans',
      lineHeight: lineHeightLabel,
      marginBottom: labelTextBottomMargin,
      paddingLeft: textPaddingLeft,
    };

    let unitTextStyle = {
      fontSize: fontSizeLabel,
      fontStyle: 'normal',
      fontFamily: 'Open Sans',
      lineHeight: lineHeightLabel,
      marginTop: unitMarginTop,
      paddingLeft: textPaddingLeft,
      marginBottom: 0,
    };

    let divStyle = {
      display: 'inline-block',
    };
    let valueClassName = 'blksail-dynamic';
    if (isRequestedValue) {
      valueClassName = 'blksail-input';
    }
    let value_text = value.toFixed(1);
    return (
      <div style={divStyle}>
        <p style={labelTextStyle} className={'blksail-label-color'}>
          {labelText}{' '}
        </p>
        <p style={valueTextStyle} className={valueClassName}>
          {value_text}
        </p>
        <p style={unitTextStyle} className={'blksail-label-color'}>
          {unitText}
        </p>
      </div>
    );
  }
}

LabelComponent.defaultProps = {};

LabelComponent.propTypes = {

    labelText: PropTypes.string,
    value: PropTypes.number,
    unitText: PropTypes.string,
    isRequestedValue: PropTypes.bool,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
