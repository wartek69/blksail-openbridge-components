import React from 'react';

import MainEngineComponent from "./MainEngineComponent";
import RudderComponent from "./RudderComponent";
import PropTypes from "prop-types";

export default {
    title: 'Rudder Component',
    component: RudderComponent,
    argTypes: {
        rudderAngleDegrees: {
            name: "Rudder Angle (degrees)",
            control: {
                type: 'range',
                min: -90,
                max: 90,
                step: 1
            }
        },
        requestedRudderAngleDegrees: {
            name: "Requested Rudder Angle (degrees)",
            control: {
                type: 'range',
                min: -90,
                max: 90,
                step: 1
            }
        },
        maxAngleDegrees : {
            name: "Maximum Rudder Angle (degrees)",
            control: {
                type: 'range',
                min: 0,
                max: 90,
                step: 1
            }
        },
        sml: {
            name: "Component Size",
            control: {
                type: 'select',
                options: ['s', 'm', 'l']
            }
        }
    },
};

const Template = (args) => <RudderComponent {...args} />;

export const Example = Template.bind({});
Example.args = {
    rudderAngleDegrees: 5,
    requestedRudderAngleDegrees: 10,
    maxAngleDegrees: 20,
    sml: 'l',
};
