import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LabelWrapperComponent from './sub-components/LabelWrapperComponent.jsx';
import RotSvgComponent from './RotSvgComponent.jsx';
import AzimuthComponent from './AzimuthComponent.jsx';
import MainEngineComponent from './MainEngineComponent.jsx';
import HdgComponent from './HdgComponent.jsx';
import RudderComponent from './RudderComponent.jsx';

/**
 * Displays rate of turn
 */
export default class BlksailMarineComponents extends Component {

    render() {
        const { id, pluginType, showLabel, sml, realValue, requestedValue, realAzimuthAngle, requestedAzimuthAngle, labelRowPriority, rotateFrame, maxAngleRudder, setProps } = this.props;
        return (
            <div
                id={id}
                style={{
                    height: '100%',
                    display: 'flex',
                    alignItems: 'stretch',
                }}
            >
                {pluginType === 'rot' && (
                    <LabelWrapperComponent
                        showLabel={showLabel}
                        sml={sml}
                        labelInfos={[
                            [
                                {
                                    title: 'Rot Real',
                                    value: realValue,
                                    isRequestedValue: false,
                                    unit: 'Percentage',
                                },
                            ],
                        ]}
                        component={<RotSvgComponent x={0} y={0} />}
                        labelRowPriority={labelRowPriority}
                    />
                )}
                {pluginType === 'rudder' && (
                    <LabelWrapperComponent
                        showLabel={showLabel}
                        sml={sml}
                        labelInfos={[
                            [
                                {
                                    title: 'Rudder Real',
                                    value: realValue,
                                    isRequestedValue: false,
                                    unit: 'Percentage',
                                },
                                {
                                    title: 'Rudder Recommended',
                                    value: requestedValue,
                                    isRequestedValue: true,
                                    unit: 'Percentage',
                                },
                            ],
                        ]}
                        component={
                            <RudderComponent
                                requestedRudderAngleDegrees={requestedValue}
                                rudderAngleDegrees={realValue}
                                maxAngleDegrees={maxAngleRudder}
                                sml={sml}
                            />
                        }
                        labelRowPriority={labelRowPriority}
                    />
                )}
                {pluginType === 'engine' && (
                    <LabelWrapperComponent
                        showLabel={showLabel}
                        sml={sml}
                        labelInfos={[
                            [
                                {
                                    title: 'Engine Real',
                                    value: realValue,
                                    isRequestedValue: false,
                                    unit: 'Percentage',
                                },
                                {
                                    title: 'Engine Recommended',
                                    value: requestedValue,
                                    isRequestedValue: true,
                                    unit: 'Percentage',
                                },
                            ],
                        ]}
                        component={
                            <MainEngineComponent
                                x={0}
                                y={0}
                                requestedThrottle={requestedValue}
                                engineThrottle={realValue}
                                rotationDegrees={0}
                                sml={sml}
                            />
                        }
                        labelRowPriority={labelRowPriority}
                    />
                )}
                {pluginType === 'hdg_compass' && (
                    <LabelWrapperComponent
                        showLabel={showLabel}
                        sml={sml}
                        labelInfos={[
                            [
                                {
                                    title: 'Heading Real',
                                    value: realValue,
                                    isRequestedValue: false,
                                    unit: 'Degrees',
                                },
                            ],
                        ]}
                        component={
                            <HdgComponent headingDegrees={realValue} rotateFrame={rotateFrame} sml={sml} />
                        }
                        labelRowPriority={labelRowPriority}
                    />
                )}
                {pluginType === 'thruster' && (
                    <LabelWrapperComponent
                        showLabel={showLabel}
                        sml={sml}
                        labelInfos={[
                            [
                                {
                                    title: 'Bow Real',
                                    value: realValue,
                                    isRequestedValue: false,
                                    unit: 'Percentage',
                                },
                                {
                                    title: 'Bow Recommended',
                                    value: requestedValue,
                                    isRequestedValue: true,
                                    unit: 'Percentage',
                                },
                            ],
                        ]}
                        component={
                            <MainEngineComponent
                                x={0}
                                y={0}
                                requestedThrottle={requestedValue}
                                engineThrottle={realValue}
                                rotationDegrees={90}
                                sml={sml}
                            />
                        }
                        labelRowPriority={labelRowPriority}
                    />
                )}
                {pluginType === 'azimuth' && (
                    <LabelWrapperComponent
                        showLabel={showLabel}
                        sml={sml}
                        labelInfos={[
                            [
                                {
                                    title: 'Power Real',
                                    value: realValue,
                                    isRequestedValue: false,
                                    unit: 'Percentage',
                                },
                                {
                                    title: 'Power Recommended',
                                    value: requestedValue,
                                    isRequestedValue: true,
                                    unit: 'Percentage',
                                },
                            ],
                            [
                                {
                                    title: 'Angle Real',
                                    value: realAzimuthAngle,
                                    isRequestedValue: false,
                                    unit: 'Percentage',
                                },
                                {
                                    title: 'Requested Real',
                                    value: requestedAzimuthAngle,
                                    isRequestedValue: true,
                                    unit: 'Percentage',
                                },
                            ],
                        ]}
                        component={
                            <AzimuthComponent
                                requestedAzimuthAngleDegrees={requestedAzimuthAngle}
                                azimuthAngleDegrees={realAzimuthAngle}
                                requestedAzimuthPowerPercentage={requestedValue}
                                azimuthPowerPercentage={realValue}
                                sml={sml}
                            />
                        }
                        labelRowPriority={labelRowPriority}
                    />
                )}
            </div>
        );
    }
}

BlksailMarineComponents.defaultProps = {};
BlksailMarineComponents.propTypes = {
    id: PropTypes.string,
    pluginType: PropTypes.string,
    showLabel: PropTypes.bool,
    sml: PropTypes.string,
    realValue: PropTypes.number,
    requestedValue: PropTypes.number,
    realAzimuthAngle: PropTypes.number,
    requestedAzimuthAngle: PropTypes.number,
    rotateFrame: PropTypes.number,
    maxAngleRudder: PropTypes.number,
    labelRowPriority: PropTypes.bool,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
