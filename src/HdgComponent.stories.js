import React from 'react';

import HdgComponent from './HdgComponent';

export default {
    title: 'Heading Component',
    component: HdgComponent,
    argTypes: {
        headingDegrees: {
            name: "Heading (degrees)",
            control: {
                type: 'range',
                min: -360,
                max: 360,
                step: 1
            }
        },
        rotateFrame: {
            name: "Rotate frame",
        },
        sml: {
            name: "Component Size",
            control: {
                type: 'select',
                options: ['s', 'm', 'l']
            }
        }
    },
};

const Template = (args) => <HdgComponent {...args} />;

export const Large = Template.bind({});
Large.args = {
    headingDegrees: 15,
    rotateFrame: false,
    sml: 'l',
};

export const LargeRotateFrame = Template.bind({});
LargeRotateFrame.args = {
    headingDegrees: 15,
    rotateFrame: true,
    sml: 'l',
};

export const Medium = Template.bind({});
Medium.args = {
    headingDegrees: 15,
    rotateFrame: false,
    sml: 'm',
};

export const Small = Template.bind({});
Small.args = {
    headingDegrees: 15,
    rotateFrame: false,
    sml: 's',
};
