import React from 'react';

import MainEngineComponent from "./MainEngineComponent";

export default {
    title: 'Main Engine Component',
    component: MainEngineComponent,
    argTypes: {
        requestedThrottle : {
            name: "Requested Throttle (%)",
            control: {
                type: 'range',
                min: -100,
                max: 100,
                step: 1
            }
        },
        engineThrottle: {
            name: "Engine Power (%)",
            control: {
                type: 'range',
                min: -100,
                max: 100,
                step: 1
            }
        },
        rotationDegrees: {
            name: "Component Rotation (optional)",
            control: {
                type: 'range',
                min: 0,
                max:360,
                step:90
            }
        },
        sml: {
            name: "Component Size",
            control: {
                type: 'select',
                options: ['s', 'm', 'l']
            }
        }
    },
};

const Template = (args) => <MainEngineComponent {...args} />;

export const Example = Template.bind({});
Example.args = {
    x: 0,
    y: 0,
    requestedThrottle: 20,
    engineThrottle: 15,
    rotationDegrees: 0,
    sml: 'l',
};
