import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class HdgNorthArrowSvgComponent extends Component {
  render() {
    if (this.props.sml === 's') {
      return (
        <svg
          x={this.props.x}
          y={this.props.y}
          width="8"
          height="9"
          viewBox="0 0 8 9"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M4 0L7.4641 8.25H0.535898L4 0Z" className="blksail-on-active" />
        </svg>
      );
    } else if (this.props.sml === 'm') {
      return (
        <svg
          x={this.props.x}
          y={this.props.y}
          width="14"
          height="16"
          viewBox="0 0 14 16"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M7 0L13.9282 15.75H0.0717969L7 0Z" className="blksail-on-active" />
        </svg>
      );
    } else if (this.props.sml === 'l') {
      return (
        <svg
          x={this.props.x}
          y={this.props.y}
          width="22"
          height="24"
          viewBox="0 0 22 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M11 0L21.3923 24H0.607696L11 0Z" className="blksail-on-active" />
        </svg>
      );
    }
    return <div>Unsupported size!</div>;
  }
}

HdgNorthArrowSvgComponent.defaultProps = {};

HdgNorthArrowSvgComponent.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
