import React, { Component } from 'react';
import PropTypes from 'prop-types';

/**
 * Renders rudder arrow
 */
export default class RudderArrowSvgComponent extends Component {
  render() {
    const { x, y, rotationDegrees, fillClassName, sml } = this.props;
    let rotate_string = 'rotate(' + -rotationDegrees + ' ' + x + ' ' + y + ')';

    if (sml === 's') {
      return (
        <g transform={rotate_string}>
          <circle cx={x} cy={y} r="4" className={fillClassName} />
          <rect x={x - 1} y={y - 1} width="2" height="32" className={fillClassName} />
        </g>
      );
    } else if (sml === 'm') {
      return (
        <g transform={rotate_string}>
          <circle cx={x} cy={y} r="12" className={fillClassName} />
          <rect x={x - 2} y={y - 2} width="4" height="104" className={fillClassName} />
        </g>
      );
    } else if (sml === 'l') {
      return (
        <g transform={rotate_string}>
          <circle cx={x} cy={y} r="12" className={fillClassName} />
          <rect x={x - 2} y={y - 2} width="4" height="208" className={fillClassName} />
        </g>
      );
    }
    return <div>Unsupported size!</div>;
  }
}

RudderArrowSvgComponent.defaultProps = {};

RudderArrowSvgComponent.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    rotationDegrees: PropTypes.number,
    fillClassName: PropTypes.string,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
