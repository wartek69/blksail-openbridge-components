import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class LineSvgComponent extends Component {
    render() {
        const { x, y, fillClassName, sml } = this.props;
        if (sml === 's') {
            return (
                <svg x={x} y={y} width="16" height="2" viewBox="0 0 16 2" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect y="-9.53674e-07" width="16" height="2" className={fillClassName} />
                </svg>
            );
        } else if (sml === 'm') {
            return (
                <svg x={x} y={y} width="40" height="4" viewBox="0 0 40 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect width="40" height="4" className={fillClassName} />
                </svg>
            );
        } else if (sml === 'l') {
            return (
                <svg x={x} y={y} width="80" height="8" viewBox="0 0 80 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect x="4.57764e-05" width="80" height="8" className={fillClassName} />
                </svg>
            );
        } else {
            return <div>Received an unsupported size!</div>;
        }
    }
}

LineSvgComponent.defaultProps = {};

LineSvgComponent.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    fillClassName: PropTypes.string,
    sml: PropTypes.string,
    /**
     * Dash-assigned callback that should be called to report property changes
     * to Dash, to make them available for callbacks.
     */
    setProps: PropTypes.func
};
